import rfko
import datetime
import sys
import pickle as pkl
import time as timing

# Get arguments
try:
    args = sys.argv[1:]
    volt = args[0]
    interval = args[1]
    rf10MHz_enable = args[2]
    time = args[3]
    n_part = args[4]
    n_turn = args[5]
    context = args[6]
except:
    raise ValueError("Invalid CLA")
else:
    print(f"> CLA FOUND {args}")

with open(f'{n_part}_{n_turn}_{context}_bchmk.pkl', 'wb') as f:
    pkl.dump(args, f)

start_time = timing.process_time()
print(f"> Running Sim at {str(start_time)}")
# Run simulation
time_taken_actual = rfko.run_rfko(
    volt = float(volt),
    interval = float(interval),
    rf10MHz_enable = rf10MHz_enable,
    time = float(time),
    n_part = int(n_part),
    n_turn = int(n_turn),
    context = context
)

print(f"> Finished Sim")
timings = {
    'volt': volt,
    'interval': interval,
    'rf10MHz_enable': rf10MHz_enable,
    'time': time,
    'n_part': n_part,
    'n_turn': n_turn,
    'platform': context,
    'duration': timing.process_time() - start_time,
    'time_taken_actual': time_taken_actual

}

with open(f'{n_part}_{n_turn}_{context}_timings.pkl', 'wb') as f:
    pkl.dump(timings, f)
print("> Saved timings to timings.pkl")
print("> Done")