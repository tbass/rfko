import numpy as np

MS_TO_S = 1e-3

start = 2 # 10^1
stop = 4  # 10^5
N = 25    # Number of points

# Generate log-linear frequencies
frequencies = np.logspace(start, stop, N)
frequency_space = frequencies

interval_param_space  = 1/frequency_space/MS_TO_S

gain_param_space = np.arange(0.1, 1.05, 0.05)

RF_ON = False
rf = '1' if RF_ON else '0'

time = 0.3
parts = 1000000
turns = 0

platform = "GPU"

with open('rfko_args.txt', 'w') as f:
    for gain in np.flip(gain_param_space):
        for interval in interval_param_space:
#                             gain,                       interval,             rf enable,      sim time,         particles,         turn override,     context
            f.write(str(round(gain, 2)) + ' ' + str(round(interval, 2)) + ' ' + str(rf) + ' ' + str(time) + ' ' + str(parts) + ' ' + str(turns) + ' ' + platform + '\n')