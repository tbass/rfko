import xtrack as xt
import xpart as xp
import xobjects as xo

import pybt

import matplotlib.pyplot as plt

import json
from numpy import random
import random
import numpy as np
import pandas as pd

from cpymad.madx import Madx

import scipy as sp

from tqdm import tqdm
import time
import datetime as dt

from signal_gen import generate_chirp_signal
import os

part = int(4e4)

N_PART = part

RAD_TO_MRAD = 1000
M_TO_MM = 1000

REL_GAMMA = 25.598474067
REL_BETA = np.sqrt(1-REL_GAMMA**(-2))

exn = 1.5e-6
eyn = 1.5e-6
ex = exn/(REL_BETA*REL_GAMMA)
ey = eyn/(REL_BETA*REL_GAMMA)

p = 5.392
momentum = p
Brho = p*3.3356

PS_radius = 70.079
DPP_FACTOR = 1e-3

## QUAD COLLAPSE

TUNE_TARGET = 0.330
#TUNE_TARGET = 1/3
CHROMATICITY_TARGET = -0.5
N_TURN = int( 5e2 )

DETUNE_QUAD_KNL1 = -0.04
# DETUNE_QUAD_KNL1 = 0

## rifko

SEPTA = True
EXCITER_SAMPLING_FREQ = 1e6

CHIRP_START = .3
CHIRP_STOP = .35

CHIRP_INTERVAL = 1 / 1000 # seconds

SEPTA_X_MM = -68.8

RFKO_STRENGTH = .5

PARENT_DIR = "../new_energy_more_particles/"

os.makedirs(PARENT_DIR, exist_ok=True)

print(f"Parent Directory: {PARENT_DIR}")
mad = Madx(stdout=False)

PS_REPO = "../../models/acc-models-ps/"

mad.call(PS_REPO+"ps_mu.seq")
mad.call(PS_REPO+"ps_ss.seq")
mad.call(PS_REPO+"scenarios/east/4_slow_extraction/ps_se_east.str")

mad.command.beam(
    particle="PROTON",
    pc = p,
    ex = ex,
    ey = ey
)
mad.input(f"BRHO      := BEAM->PC * 3.3356;")
mad.use(sequence="PS")
mad.save(sequence='PS', file=f"ps_{dt.datetime.now().strftime('%d%b')}.seq")