import numpy as np

MS_TO_S = 1e-3

# worst case: low voltage, low interval

volt = 0.1
interval = 0.1

RF_ON = False
rf = '1' if RF_ON else '0'

time = 0.5

parts = np.logspace(0, 7, 15)

turns = np.logspace(0, 6, 13)

platforms = ['CPU']

# say its 20s/it for 500000 particles
s_per_it_per_part = 30 / 500000

with open('benchmark_args.txt', 'w') as f:
    for platform in platforms:
        for turn in turns:
            for part in parts:
                if platform == "GPU":
                    # cap at 10 hours
                    est_eta = 120 + int(int(part)*int(turn)*0.0000005)
                    eta = min(est_eta, 36000)
                else:
                    # 5286 seconds for 3000 particles and 100000 turns
                    # cap at 10 hours
                    eta_p_t_p_p = 5286 / (3000 * 100000)
                    est_eta = 240 + int(int(part)*int(turn)*eta_p_t_p_p)*2
                    eta = min(est_eta, 36000)
    #                             gain,                       interval,             rf enable,      sim time,         particles,              turn override,          context
                f.write(str(round(volt, 2)) + ' ' + str(round(interval, 2)) + ' ' + str(rf) + ' ' + str(time) + ' ' + str(int(part)) + ' ' + str(int(turn)) + ' ' + platform + ' ' + str(eta) + '\n')