# RFKO on Xsuite

This repo contains the simulation code for RFKO Slow Extraction at PS East Area using Xsuite. It is structured to use CERN's LXPlus HTCondor Batch service, and relies on the Docker images found at https://gitlab.cern.ch/tbass/xsuite-docker

## `rfko.py`

The main simulation file is structured as a module: 
- running `import rfko` will use **cpymad** to load the sequence `ps_24May.seq`, match tunes, and slice+thin the lattice
- running `rfko.run_rfko()` will then use **Xsuite** to convert the cpymad lattice, install custom elements, generate a particle distribution, and perform tracking

Because it is structured to be run as a batch jobs, parameters are passed via **command line arguments** by the python script's manager, `rfko.sh`. These arguments are:
- `volt` [arb]: the Vpp voltage/gain across the TFB damper plates, between 0 and 1
- `interval` [ms]: the repetition rate of the chirp signal in milliseconds
- `rf10MHz_enable`: a flag, `'0'` or `'1'`, to enable or disable (set voltage to zero) the 10 MHz RF system, i.e. to bunch or debunch the particles
- `time` [s]: the total simulation time in seconds; typically 0.5
- `n_part` [int]: the number of particles to be generated. 500,000 work well
- `n_turn` [int]: an override for `time`, explicitly setting the number of turns rather than using `time*frev`
- `context`: whether to run the simulation on the CPU (`'CPU'`) or CUDA GPU (`'GPU'`)

Once the file is imported and the cpymad line is generated, the `run_rfko()` function takes these as either direct arguments, or **command line arguments (CLA)** (sys.argv) in the order above. If it fails to find the right number of CLAs, it will default to the arguments passed with `run_rfko(**kwargs)`, or its default values.

The function will then:
1. import the line from cpymad
2. configure the reference particle as a proton, charge 1, momentum 5.392 GeV/c
3. run a twiss to obtain frev; this should be ~470kHz
4. calculate `n_turns` from `frev*time`, or explicit override
5. create a septum aperture at `x=-60mm`
6. calculate the angular kick from the TFB plates
7. generate the chirp signal from `signal_gen.py` and create an `Exciter` element with this signal and kick
8. create an RF cavity at the 8th harmonic with 130kV
9. Generate a matched particle distribution
10. run the RFKO simulation

Outputs are saved into a Pickle file, named `<volt>_<interval>_results.pkl`. It is named this way to prevent multiple simulation jobs being overwritten. It looks like:
- `"bincount"`: a `np.bincount` of the turn number particles were lost to apertures; this produces a good approximation of spill signal
- `"constants"`: various parameters, states, and a total time taken for the simulation
- `"at_turn"`: raw particles.at_turn data
- `"zeta"`: raw particles.zeta data
- `"particles"`: a large pandas dataframe of the particle state at the end of simulation. This can be used to reconstruct the beam spill.
  